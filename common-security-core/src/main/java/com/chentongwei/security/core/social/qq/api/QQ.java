package com.chentongwei.security.core.social.qq.api;

/**
 * QQ接口
 *
 * @author chentongwei@bshf360.com 2018-04-02 09:53
 */
public interface QQ {

    QQUserInfo getUserInfo();

}
