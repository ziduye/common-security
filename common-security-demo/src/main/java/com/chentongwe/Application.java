package com.chentongwe;

import com.chentongwe.security.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author chentongwei@bshf360.com 2018-03-26 10:26
 */
@ServletComponentScan
@ComponentScan(basePackages = {"com.chentongwe", "com.chentongwei"})
@SpringBootApplication
@RestController
public class Application extends WebMvcConfigurerAdapter{
    /**
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @GetMapping("/hello")
    public String hello() {
        return "hello spring security";
    }

    @GetMapping("/hello2")
    public String hello2() {
        return "hello spring security";
    }

    @Autowired
    private MyUserDetailsService userDetailsService;

    @PostMapping("/login123")
    public UserDetails login(String username) {
        System.out.println("login进来了");
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        return userDetails;
    }


}
